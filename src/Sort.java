abstract class Sort{

    //int best, avg, worst;
    private static int durch, swap;

    public static int getDurch(){
        return durch;
    }

    public static int getSwap(){
        return swap;
    }

    public void init(){
        swap = 0;
        durch = 0;
    }



    public static boolean less(Comparable v, Comparable w){
        durch += 1;
        return v.compareTo(w) < 0;
    }
    public static void exch(Comparable[] a, int i, int j){
        swap += 1;
        Comparable swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

/*    @Override
    public int compareTo(Object o) {
        return 0;
    }*/
}
